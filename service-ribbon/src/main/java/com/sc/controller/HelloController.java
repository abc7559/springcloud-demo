package com.sc.controller;

import com.sc.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloController {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String hello() {
        log.info("hello  ");
        return "hi";
    }

    @RequestMapping(value = "hi", method = RequestMethod.GET)
    public String hello(@RequestParam(value = "name" ,  defaultValue = "hello") String name) {
        log.info("hello {} ",   name  );
        return helloService.helloService(name);
    }
}
