package com.sc.controller;

import com.sc.service.SayHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SayHelloController {

    @Autowired
    SayHelloService sayHelloService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String sayHello() {
        return "hello this is feign";
    }

    @RequestMapping(value = "hi", method = RequestMethod.GET)
    public String sayHelloRoot(@RequestParam String name) {
        return sayHelloService.sayHelloClientOne(name);
    }



}
