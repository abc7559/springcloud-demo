package com.sc.client2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class Client2Application {

    @Value("${server.port}")
    private String port;

    public static void main(String[] args) {
        SpringApplication.run(Client2Application.class, args);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Object hi() {
        return "My server port is :" + port;
    }

    @RequestMapping(value = "hi", method = RequestMethod.GET)
    public Object hiRoot(@RequestParam(value = "name" ,  defaultValue = "c1") String name) {
        return "c1   hi my name is : " + name + "   My server port is :" + port;
    }


}
